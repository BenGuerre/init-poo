import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CarteCouleur implements Carte {
    
    private String couleur;// "Coeur", "Carreau", "Pique", "Trèfle" 
    private int valeur;
    static List<String> listeCouleur = new ArrayList<String>(Array.asList({"As", "2", "3", "4", "5", "6", "7"," 8", "9", "10", "Valet", "Dame", "Roi"}));

    public CarteCouleur(int valeur, String couleur) {
        this.couleur = couleur;
        this.valeur = valeur;
        
    }  
    
    @Override 
    public String getValeur(){
        String carte = "";
        if( this.valeur == 1){
            carte = "As";
        }
        else if( this.valeur == 11){
            carte = "Valet";
        }
        else if( this.valeur == 12){
            carte = "Dame";
        }
        else if( this.valeur == 13){
            carte = "Roi";
        }
        else{
            carte = Integer.toString(this.valeur);
        }
        return carte;    
    }
    
    @Override
    public int getValeurInt(){ 
        return this.valeur;
    }

    @Override
    public String getType() {
        /*
        Retourne la couleur
        */
        return this.couleur;
    }

    @Override 
    public String toString(){
        return this.getValeur() + " de " + this.couleur;
    }
}   
