class ExecutableMagasin{
    public static void main(String[] args) {
        Magasin mag1 = new Magasin("auchan", true, false);
        Magasin mag2 = new Magasin("lanini", true, true);
        Magasin mag3 = new Magasin("carouf", false, true);

        System.out.println(mag1.getNom());
        System.out.println(mag2.getOuvertDimanche());
        System.out.println(mag3.getOuvertLundi());
        System.out.println(mag1.toString());
        System.out.println(mag3.toString());

        ListeMagasins listeMag = new ListeMagasins();

        listeMag.ajoute(mag1);
        listeMag.ajoute(mag2);
        listeMag.ajoute(mag3);

        System.out.println(listeMag.ouvertsLeLundi());
        System.out.println(listeMag.toString());

        System.out.println("----------------dicoMag----------------");
        DicoMagasins dicoMag = new DicoMagasins();
        dicoMag.ajoute("mag1" , true, false);
        dicoMag.ajoute("mag2" , true, true);
        dicoMag.ajoute("mag3" , false, true);
        System.out.println(dicoMag);
        
        System.out.println("----------------ouvert lundi----------------");
        System.out.println(dicoMag.ouvertsLeLundi());

    }
}