class ExecutablePersonnage {
    public static void main(String [] arg){
        Personnage pers1 = new Personnage("karim");
        Personnage pers2 = new Personnage("toto", 10);
        Personnage pers3 = new Personnage("Jack", 0);

        System.out.println("-----------------affiche des personnages-----------------");
        System.out.println(pers1.toString());
        System.out.println(pers2.toString());
        System.out.println(pers3.toString());
    }
}
