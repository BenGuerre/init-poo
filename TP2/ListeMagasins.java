import java.util.ArrayList ;
import java.util.List ;

public class ListeMagasins {
   private List<Magasin> magasins;

   public ListeMagasins() {
        this.magasins = new ArrayList<>();
    }

   public void ajoute(Magasin mag) {
       this.magasins.add(mag);}

   public List<Magasin> ouvertsLeLundi() {
    ArrayList<Magasin> magasinsOuvertLundi =  new ArrayList<>();
    for (Magasin varmag : this.magasins){
        if (varmag.getOuvertLundi()){
            magasinsOuvertLundi.add(varmag);
        }
    }
    return magasinsOuvertLundi;
    }

   @Override
   public String toString() {
       return magasins.toString();      
    }


}