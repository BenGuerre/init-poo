import java.util.ArrayList;
public class ExecutableMin{
    public static void main(String[] args){
        ArrayList<Integer> liste;
        liste = new ArrayList<Integer>();
        Integer min = LibEntiers.min(liste); // null
        System.out.println(min);
        liste.add(130);
        liste.add(20);
        liste.add(12);
        liste.add(1200000);
        min = LibEntiers.min(liste); // 12
        System.out.println(min);
    }
}
