public class Personne{
    private String prenom;

    public Personne(String prenom){
        this.prenom = prenom;
    }
    
    public String getNom(){
        return this.prenom;
    }

    @Override
    public String toString(){
        return this.prenom;
    }
}