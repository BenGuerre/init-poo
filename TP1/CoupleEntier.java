class CoupleEntiers {
    private int premier, second;
    public CoupleEntiers() {
        this.premier = 0;
        this.second = 0;
    }
    public void setPrem(int premier) {
        this.premier = premier;
    }
    public void setSec(int second){
        this.second = second;
    }
    public void permute() {
        int aux;
        aux = this.premier;
        this.premier = this.second;
        this.second = aux;
    }
    public int fraction() {
        if(this.second != 0){
            return this.premier / this.second;
        }
        return 0;      
    }
    public String toString(){ //Permet d'afficher l'objet 
        return "(" + this.premier + "," + this.second + ")";
    }
    public int getPrem(){
        return this.premier;
    }
    public int getSec(){
        return this.second;
    }
    public int somme(){
        return getPrem() + getSec();
    }
    public CoupleEntiers plus(CoupleEntiers couple1, CoupleEntiers couple2){
        CoupleEntiers res = new CoupleEntiers();
        res.setPrem(couple1.getPrem() + couple2.getPrem());
        res.setSec(couple1.getSec() + couple2.getSec());
        return res;
    }
 }
 
 
