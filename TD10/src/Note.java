package src;

public class Note{
         // attributs privés
    private int duree;
    private int frequence;

    public Note(int frequence, int duree){
        this.frequence = frequence;
        this.duree = duree;
    }

    public int getDuree() {
        return duree;
    }
    public int getFrequence() {
        return frequence;
    }

    public void jouer(){
        System.out.println("Je joue la note de fréqunce " + this.frequence + " pendant " + this.duree);
    };

    @Override
    public int hashCode() {
        return this.duree + this.frequence;
    } // ceci permet de les mettre dans un HashSet.

    @Override
    public boolean equals(Object o){
        if(o == null || !(o instanceof Note)){return false;}
        if (o  == this){return true;}
        Note note = (Note) o;
        return (this.duree  == note.duree && this.frequence == note.frequence);
    }

    @Override
    public String toString() {
        return  "La note de fréquence" + this.frequence + " dure " + this.duree;
    }

    public static void main(String[] args) {
        Note a = new Note(3, 2);
        Note b = new Note(1, 2);
        Note c = new Note(3, 2);

        System.out.println(!a.equals(b));
        System.out.println(a.equals(c));
        System.out.println(!(a.hashCode() == b.hashCode()));
        System.out.println(a.hashCode() == c.hashCode());
    }
}